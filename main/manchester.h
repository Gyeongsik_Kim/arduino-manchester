#define __MANCHESTER_H__

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
  #include <pins_arduino.h>
#endif

typedef enum {WRITE_PACKET = 32, REQUEST_PACKET = 16} PACKET_TYPE;

typedef struct {
  union{
    union{
      struct{
        uint16_t sync : 2;
        uint16_t isRead : 1;
        uint16_t chipSelect : 4;
        uint16_t addr : 6;
        uint16_t crc : 3;
      } requestData;
      struct{
        uint32_t sync : 2;
        uint32_t isRead : 1;
        uint32_t chipSelect : 4;
        uint32_t addr : 6;
        uint32_t code : 16;
        uint32_t crc : 3;
      };
    }
    uint8_t data[4];
  };
  PACKET_TYPE type;
} Packet;

typedef struct {
  Packet* packet;
  uint16_t position;
  boolean isDone;
} ReadData;

typedef struct {
  Packet* packet;
  uint16_t position;
  boolean isDone;
} WriteData;

class Manchester{
  public:
    Manchester(int rx, int tx);
    Manchester();
    ReadData* read();
    void write(Packet*);
    Packet* initPacket();
};
