#include "manchester.h"

Manchester man;

void setup() {
  Serial.begin(9600);
  Packet* packet = man.initPacket();
  man.write(packet);
  Serial.println("Access Code");
  packet->code = 0x0;
  Serial.println("NEXT PACKET");
  man.write(packet);
  Serial.println("Exit Code");
  packet->requestData->crc = 0;
  packet->type = REQUEST_PACKET;
  ReadData* readValue = man.read();
  char buf[20];
  sprintf(buf, "\\x%02x\\x%02x\\x%02x\\x%02x", 
    readValue->packet->data[0],readValue->packet->data[1],readValue->packet->data[2],readValue->packet->data[3]);
  Serial.print("RECEIVE DATA");
  Serial.println(buf);

  packet->chipSelect = 0x0;
  packet->addr = 0x1b;
  packet->data = 0x1b1c;
  packet->crc = 0x3;
  packet->type = WRITE_PACKET;
  man.write(packet);
  delete packet;
}

void loop() {
  Packet* packet = man.initPacket();
  man.write(packet);
  delete packet;
}

