#include "manchester.h"

//야근 방지 코드
//little endian
/**
• Manchester Access Code
• Manchester Exit Code
• Read
• Read Response
• Write
**/


int readCount, writeCount;
int readPin, writePin;
ReadData* readData;
WriteData* writeData;
volatile bool sending;

ISR(TIMER0_COMPA_vect){
  //수신
  int value = digitalRead(readPin);
  
  if(readCount > -1)
    readCount++;  
  
  //HIGH인 경우
  if(value == HIGH){
    if(readData == nullptr){
      readData = new ReadData;
      readData->packet = new Packet;
      readData->isDone = false;
      readData->position = 0;
      readCount = 1;
      //짝수면 HIGH, 홀수면 LOW
    }else{
      if(!readData->isDone){
        readData->position++;

        if(readData->position > 31){
          readData->isDone = true;
        }

        if(readCount & 1){
          uint32_t tt = ((uint32_t)0x1 << (32 - readData->position));
          tt = ((uint32_t)((uint32_t)readData->packet->data[0] << 24 | (uint32_t)readData->packet->data[1] << 16 | (uint32_t)readData->packet->data[2] << 8 | (uint32_t)readData->packet->data[3])) | tt;
          memcpy(readData->packet->data, &tt, sizeof(uint32_t));
        }
      }
    }
  }
}

ISR(TIMER2_COMPA_vect){
  //전송
  if(writeCount > -1)
    writeCount++;
  
  if(writeData != nullptr){
    sending = true;
    if(!writeData->isDone){
      //시작점마다 ++
      if(!(writeCount & 1))
        writeData->position++;
      if(writeData->position > writeData->packet->type){
        Serial.println("Send Done");
        sending = false;
        writeData->isDone = true;
        return;
      }
      Serial.print("Send");
      Serial.print(writeData->position);
      uint32_t value = 0;
      memcpy(&value, writeData->packet->data, sizeof(uint32_t));
      //짝수면 HIGH, 홀수면 LOW
      if(value & ((uint32_t)0x1 << (writeData->packet->type - writeData->position))){
        //1인 경우
        digitalWrite(writePin, (writeCount & 1) ? LOW : HIGH);
        Serial.print('\t');Serial.print((writeCount & 1) ? LOW : HIGH);Serial.print('\t');Serial.println(1);
       
      }else{
        //0인 경우
        digitalWrite(writePin, (writeCount & 1) ? HIGH : LOW);
        Serial.print('\t'); Serial.print((writeCount & 1) ? HIGH : LOW); Serial.print('\t'); Serial.println(0);
      }
    }else{
      sending = false;
      delete writeData->packet;
      delete writeData;
      writeData = nullptr;
      writeCount = -9999;
      Serial.println("WRITEDATA NULLPTR");
    }
  }else{
    digitalWrite(writePin, LOW);
  }
}

Manchester::Manchester(int rx, int tx) {
  readPin = rx;
  writePin = tx;
  pinMode(readPin, INPUT);
  pinMode(writePin, OUTPUT);
  sending = false;
  readCount = writeCount = 0;

  //데이터들 초기화
  readData = nullptr;
  writeData = nullptr;

  #ifdef DEBUG
    Serial.print("Input Pin : "); Serial.println(rx);
    Serial.print("Output Pin : "); Serial.println(tx);
  #endif

  cli();

  TCCR0A = 0;
  TCCR0B = 0;
  TCNT0  = 0;
  OCR0A = 249;
  TCCR0A |= (1 << WGM01);
  TCCR0B |= (1 << CS01);
  TIMSK0 |= (1 << OCIE0A);

  TCCR2A = 0;
  TCCR2B = 0;
  TCNT2  = 0;
  OCR2A = 249;
  TCCR2A |= (1 << WGM21);
  TCCR2B |= (1 << CS21);
  TIMSK2 |= (1 << OCIE2A);

  sei();
}

Manchester::Manchester() {
  ::Manchester(8, 9);
}

ReadData* Manchester::read(){
  while(readData == nullptr);
  while(!readData->isDone);

  ReadData* data = new ReadData;
  memcpy(data, readData, sizeof(ReadData));

  data->packet = new Packet;
  memcpy(data->packet, readData->packet, sizeof(Packet));

  delete readData->packet;
  delete readData;
  
  readData = nullptr;
  readCount = -9999;
  return data;
}

void Manchester::write(Packet* packet){
  while(sending);
  Serial.println("READY SEND");
  writeData = new WriteData;
  writeData->packet = new Packet;
  writeData->position = -1;
  writeData->isDone = false;
  writeCount = 1;
  memcpy(writeData->packet, packet, sizeof(Packet));
  Serial.println("SEND GO");
}

Packet* Manchester::initPacket(){
  Packet* data = new Packet;
  memset(data, 0x00, sizeof(Packet));
  data->crc = 0x3;
  data->addr = 0x3f;
  data->code = 0x62d2;
  data->type = WRITE_PACKET;
  return data;
}
