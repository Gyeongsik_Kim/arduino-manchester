#include "manchester.h"

//야근 방지 코드
//little endian

static int readCount, writeCount;
static int readPin, WritePin;

ISR(TIMER0_COMPA_vect){
  //수신
  int value = digitalRead(readPin);
  
  if(readCount > -1)
    readCount++;  
  
  //HIGH인 경우
  if(value == HIGH){
    if(readData == nullptr){
      readData = new ReadData;
      readData->data = new Packet;
      readData->isDone = false;
      readData->position = 0;
      readCount = 1;
      //짝수면 HIGH, 홀수면 LOW
    }else{
      if(!readData->isDone){
        readData->position++;

        if(readData->position > 31){
          readData->isDone = true;
        }

        if(count & 1){
          readData->data->data |= ((uint32_t)0x1 << (32 - readData->position));
        }
      }
    }
  }
}

ISR(TIMER2_COMPA_vect){
  //전송
  if(writeCount > -1)
    writeCount++;
  
  if(writeData != nullptr){
    if(!writeData->isDone){
      //시작점마다 ++
      if(!(writeCount & 1))
        writeData->position++;
      if(writeData->position > 31){
        writeData->isDone = true;
        return;
      }
      //짝수면 HIGH, 홀수면 LOW
      if(writeData->data->data & ((uint32_t)0x1 << (32 - writeData->position))){
        //1인 경우
        digitalWrite(writePin, (writeData->position & 1) ? LOW : HIGH);
      }else{
        //0인 경우
        digitalWrite(writePin, (writeData->position & 1) ? HIGH : LOW);
      }
    }else{
      delete writeData->data;
      delete writeData;
      writeData = nullptr;
      writeCount = -9999;
    }
  }else{
    digitalWrite(writePin, LOW);
  }

}

void Manchester::begin(int rx, int tx) : readPin(rx), writePin(tx){

  data = NULL;
  size = now = -1;
  pinMode(readPin, INPUT);
  pinMode(writePin, OUTPUT);

  readCount = writeCount = 0;

  //데이터들 초기화
  readData = nullptr;
  writeData = nullptr;

  #ifdef DEBUG
    Serial.print("Input Pin : "); Serial.println(rx);
    Serial.print("Output Pin : "); Serial.println(tx);
  #endif

  cli();

  TCCR0A = 0;
  TCCR0B = 0;
  TCNT0  = 0;
  OCR0A = 249;
  TCCR0A |= (1 << WGM01);
  TCCR0B |= (1 << CS01);
  TIMSK0 |= (1 << OCIE0A);

  TCCR2A = 0;
  TCCR2B = 0;
  TCNT2  = 0;
  OCR2A = 249;
  TCCR2A |= (1 << WGM21);
  TCCR2B |= (1 << CS21);
  TIMSK2 |= (1 << OCIE2A);

  sei();
}

void Manchester::begin() : begin(8, 9);

ReadData* Manchester::read(){
  while(readData == nullptr);
  while(!readData->isDone);

  ReadData data = new ReadData;
  data->data = new Packet;

  memcpy(data->data, readData->data, sizeof(Packet));
  memcpy(data, readData, sizeof(ReadData));

  delete readData->data;
  delete readData;
  
  readData = nullptr;
  readCount = -9999;
  return data;
}

void Manchester::write(Packet* packet){
  while(writeData != nullptr);
  writeData = new WriteData;
  writeData->data = new Packet;
  writeData->position = 0;
  writeData->isDone = false;
  writeCount = 1;
  memcpy(writeData->data, packet, sizeof(Packet));
}

Packet* initWriteData(){
  Packet* data = new Packet;
  data->crc = 0x3;
  data->nothing2 = 0x3f;
  data->code = 0x62d2;
  return data;
}