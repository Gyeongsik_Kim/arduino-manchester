#define __MANCHESTER_H__

#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
  #include <pins_arduino.h>
#endif


typedef struct {
  union{
    struct{
      long sync : 2;
      long nothing : 1;
      long chipSelect : 4;
      long nothing2 : 6;
      long code : 16;
      long crc : 3;
    };
    uint8_t data[4];
  };
} Packet;

typedef struct {
  Packet data;
  uint16_t position;
  boolean isDone;
} ReadData;

typedef struct {
  Packet data;
  uint16_t position;
  boolean isDone;
} WriteData;

volatile ReadData* readData;
volatile WriteData* writedata;

class Manchester{
  public:
    void begin(int rx, int tx);
    void begin();
    ReadData* read();
    void write(Packet*);
    Packet* initPacket();
};
